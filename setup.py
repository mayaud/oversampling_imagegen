from setuptools import setup, find_packages

setup(
    name='oversamplingImageGen',
    version='0.1',
    packages=find_packages(),
    description='A package to generate new images from existing ones to balance the dataset',
    author='Victor MAYAUD',
    author_email='mayaud@eurecom.fr',
    url='https://github.com/mayaud/oversampling_imageGen'
)
