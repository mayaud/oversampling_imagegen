# oversampling_imageGen (Image Generator)

![Image Transformation](draw.png "Oversampling example on image 32x32")

## Overview

The ImageGenerator class is designed for image oversampling tasks. It supports the addition of Gaussian noise, rotation, luminosity adjustment, and zooming in or out on images. Currently, the class is configured to work only with .jpg images. This tool is particularly useful in augmenting datasets for machine learning models, especially in scenarios where data diversity is needed to train robust models.


## Installation

To use the ImageGenerator class, you need Python installed on your machine along with some specific libraries. Here's how you can set it up:

## Prerequisites

1. Python 3.x
2. NumPy
3. OpenCV
4. Pillow
5. fastai (optional for image file utility)

## Steps to Install Required Libraries

You can install the necessary Python libraries using pip. Run the following command in your terminal:

```bash
$ pip install numpy opencv-python pillow fastai
```

Then do this for installing imageGen

```bash
$ pip install git+https://gitlab.eurecom.fr/mayaud/oversampling_imagegen.git
```
# Usage

## Getting Started

First, make sure you have images in .jpg format stored in a directory. You will pass the path of this directory when creating an instance of ImageGenerator.

## Example Code

Here is a basic example of how to use the ImageGenerator:

```python
from oversamplingImageGen import ImageGenerator  

# Initialize the generator with the path to your images
image_path = '/path/to/your/images'
generator = ImageGenerator(image_path)

# Normalize images
generator.normalize()

# Generate images with modifications
# Here, we generate 10 images and save them to a specified path
generated_images = generator.gen_images(10, save_path='/path/to/save/images', save=True)

print("Generated images have been saved.")
```

## Functions

1. normalize(): Prepares the images by normalizing their pixel values. This step is required before generating new images.

2. gen_images(number, save_path=None, save_name="generated_image", noise=True, rotate=True, luminosity=True, zoom=True, save=False): Generates specified number of images with possible modifications. If save is set to True, it will save the images to the specified path.

## Parameters

Only the function gen_images has parameters:

1. number: Number of images to generate.
2. save_path: Path where the generated images will be saved. Required if save is True.
3. save_name: Base name for saved images, appended with an index.
4. noise, rotate, luminosity, zoom: Boolean flags to specify which augmentations to apply.
5. save: Whether to save the generated images.

## Contributing

Contributions are welcome. If you have suggestions or improvements, please fork the repository and submit a pull request.
