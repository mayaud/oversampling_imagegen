import numpy as np
from fastai.vision.utils import get_image_files
from PIL import Image
import cv2

"""
This class is used for oversampling images by adding noise, 
rotating, changing luminosity and zooming in/out.
Only jpg images are supported.
"""


### Class Definition ###
class ImageGenerator:
    class Image_gen:
        def __init__(self, image) -> None:
            self.image = image

        def add_noise(self, variance=0.1, mean=0):
            """Add Gaussian noise to an image"""
            noise = np.random.normal(mean, variance, self.image.shape)
            self.image += noise
            self.image = np.clip(self.image, 0., 1.)

        def add_rotate(self, angle=90):
            """Rotate an image by a given angle in degrees"""
            self.image = np.rot90(self.image, k=angle // 90)
            return self.image

        def add_luminosity(self, factor=1):
            """Change the luminosity of an image"""
            self.image = np.clip(self.image * factor, 0., 1.)
            return self.image

        def add_random_zoom(self, min_factor=0.5, max_factor=1.5):
            """Apply random zooming within a range to an image."""
            if min_factor >= max_factor:
                raise ValueError("min_factor must be less than max_factor")
            factor = np.random.uniform(min_factor, max_factor)
            h, w = self.image.shape[:2]
            new_h = int(h * factor)
            new_w = int(w * factor)
            if factor < 1.0:
                top = np.random.randint(0, np.abs(h - new_h))
                left = np.random.randint(0, np.abs(w - new_w))
                cropped = self.image[top:top + new_h, left:left + new_w]
                self.image = cv2.resize(cropped, (w, h), interpolation=cv2.INTER_LINEAR)
            elif factor > 1.0:
                resized = cv2.resize(self.image, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
                top = np.random.randint(0, np.abs(new_h - h))
                left = np.random.randint(0, np.abs(new_w - w))
                self.image = resized[top:top + h, left:left + w]
            return self.image

    def __init__(self, path) -> None:
        self.images = get_image_files(path)
        self.size = len(self.images)
        self.normalized_images = []

    def normalize(self):
        """Normalize the images by dividing by 255"""
        print("Processing images normalization")
        counter = 0
        for image_path in self.images:
            if counter % 500 == 0:
                print(f"Processing image {counter}/{self.size}")
            im = Image.open(image_path)
            im_array = np.array(im)
            im_array = im_array.astype('float32') / 255.0
            self.normalized_images.append(im_array)
            counter += 1
        print("Processing done")

    def gen_images(self, number, save_path=None, save_name="generated_image", noise=True, rotate=True, luminosity=True, zoom=True, save=False):
        """Generate images with optional modifications and optionally save them."""
        generated_images = []
        counter = 0
        
        #error if no normalized images
        if not self.normalized_images:
            raise ValueError("Please normalize the images first with the function self.normalize()")

        for i in range(number):
            if counter%100==0:
                print(f"Generating image {counter}/{number}")
            #choose a random image
            k = np.random.randint(0, len(self.normalized_images))
            im = self.normalized_images[k]
            img_gen = self.Image_gen(im.copy())
            
            if noise:
                img_gen.add_noise(variance=np.random.uniform(0, 0.1))
            if rotate:
                img_gen.add_rotate(angle=np.random.choice([90, 180, 270]))
            if luminosity:
                img_gen.add_luminosity(factor=np.random.uniform(0.5, 1.5))
            if zoom:
                img_gen.add_random_zoom(min_factor=0.5, max_factor=1.5)

            if save:
                if not save_path:
                    raise ValueError("Please provide a save path")
                normalized_image = np.clip(img_gen.image * 255, 0, 255).astype(np.uint8)
                normalized_image = cv2.cvtColor(normalized_image, cv2.COLOR_BGR2RGB)
                complete_file_path = f"{save_path}/{save_name}_{i}.jpg"
                cv2.imwrite(complete_file_path, normalized_image)
            
            counter+=1
            generated_images.append(img_gen.image)

        return generated_images
